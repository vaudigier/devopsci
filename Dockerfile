FROM openjdk:11
COPY src /src

RUN javac src/main/java/com/epsi/devops/Main.java -d bin
CMD java -cp bin com.epsi.devops.Main