# Lancer un pipeline

 - Aller dans l'onglet CI/CD
 - Appuyer sur run pipeline

# Lancer le projet dans un docker

- Ouvrir un terminal à la racine du projet
- ``docker build -t dockerization .``
- ``docker run --name dockerization dockerization``
- ``docker start dockerization``