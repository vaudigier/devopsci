package com.epsi.devops;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMain {

    @Test
    public void mainHelloTest() {
        assertEquals(Main.hello(), "Hello world!");
    }
}
